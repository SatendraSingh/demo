//
//  DataManager.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 09/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
import CoreData
import UIKit
class DataManager {
    var User : [NSManagedObject] = []
    static var UserDetails = UserDataModel()
    func save(_ email : String, _ password : String, _ firstName: String, _ lastName : String) -> Bool? {
        //get reference to the app delegate
      guard let appDelegate =
         UIApplication.shared.delegate as? AppDelegate else {
            return nil
       }
        self.retrieveData()
        for elem in User {
            if elem.value(forKey: "email") as! String == email{
                return nil
            }
        }
       
       // Step 1 : get the NSManagedContext to work with the managed objects
        // There are two steps involved in persisting a new managed object --
        // 1. insert the new obj into a managed object context
        // 2. commit the changes in the managed obj context to save it on disk
       let managedContext =
         appDelegate.persistentContainer.viewContext
       
       // Step 2 (step 1 to save data) : Create a managed object and insert it into the managed obj context
        //Returns the entity with the specified name from the managed object model associated with the specified managed object context’s persistent store coordinator.
        //NSEntity Description is the piece linking the entity defn from the data model with an instance of NSManagedObj at runtime
       let entity =
         NSEntityDescription.entity(forEntityName: "User",
                                    in: managedContext)!
       //base class that implements the behaviour required of a core data model object
         //NSManagedObject represents a single object stored in Core - Data
         //It can take the form of any entity in the data model
       let user = NSManagedObject(entity: entity,
                                    insertInto: managedContext)
       
       //Step 3 : We have the managed obj -- user
        //Set the various attributes of the entity
        user.setValue(email, forKeyPath: "email")
        user.setValue(password, forKey: "password")
        user.setValue(firstName, forKey: "firstname")
        user.setValue(lastName, forKey: "lastname")
       // Step 4 (step 2 to save data): commit the changes made to user to the context
       do {
         try managedContext.save()
        return true
         //people.append(person)
       } catch let error as NSError {
         print("Could not save. \(error), \(error.userInfo)")
       }
        return false
    }
    
    func verifyLogin(_ email : String, _ password : String) -> Bool{
        self.retrieveData()
        for elem in User{
            if elem.value(forKey: "email") as! String == email && elem.value(forKey: "password") as! String == password{
                //set details of logged in user
               // DataManager.UserDetails.email = email
                //DataManager.UserDetails.firstName = elem.value(forKey: "firstname") as! String
               // DataManager.UserDetails.lastName = elem.value(forKey: "lastname") as! String
                return true
            }
        }
        return false
    }
    
    func getLoggedInUserDetails(_ email : String){
        self.retrieveData()
        for elem in User{
            if elem.value(forKey: "email") as! String == email{
                DataManager.UserDetails.email = email
                DataManager.UserDetails.firstName = elem.value(forKey: "firstname") as? String
                DataManager.UserDetails.lastName = elem.value(forKey: "lastname") as? String
            }
        }
    }
    func retrieveData(){
        //UIApplication.shared returns the singletion app instance
        //.delegate provides the delegate of the app instance
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{
            return
        }
        //get the managed Context to work with managed objects
        let managedContext = appDelegate.persistentContainer.viewContext
        //get data -- NSFetchRequest is responsible for fetching from Core data
        //this is basically the query
        //NSFetch request is a generic type -- here NSManagedObject specifies the return type
        let fetch = NSFetchRequest<NSManagedObject>(entityName: "User")
        
        //fetch returns an array of managed objects that meets the criteria specified by the fetch request
        //this fetch request is handed over to the managed context
        do{
            User = try managedContext.fetch(fetch)
        }
        catch{
            print(error)
        }
}
    func update(_ email : String,_ firstName: String, _ lastName : String, _ profilePicture : Data?) -> Bool?{
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{
            return nil
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "User")
        //specify the predicate -- look for the user with the specified email
        //%@ indicates place the contents of variable here, whatever is the data type
        fetchRequest.predicate = NSPredicate(format: "email == %@", email)
        do{
            let userData = try managedContext.fetch(fetchRequest)
            print(userData)
            let objectToUpdate = userData[0] as! NSManagedObject
            objectToUpdate.setValue(email, forKey: "email")
            objectToUpdate.setValue(firstName, forKey: "firstname")
            objectToUpdate.setValue(lastName, forKey: "lastname")
            if let image = profilePicture{
                objectToUpdate.setValue(image, forKey: "profilePicture")
            }
            do{
                try managedContext.save()
                return true
            }
            catch
            {
                print("Save error",error)
            }
        }
        catch{
            print(error)
        }
        return false
    }
}
