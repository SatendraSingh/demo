//
//  HeaderReusableView.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 08/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit

//By default the header and footer views are assosiacted with the UICollectionResuableView
//to display the title on the header, we need to subclass it
class HeaderReusableView: UICollectionReusableView {
        
    @IBOutlet weak var title: UILabel!
}
