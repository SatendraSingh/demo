//
//  VideoDataModel.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
struct RootElement: Codable {
    let title: String
    let nodes: [Node]
}

struct Node: Codable {
    let video: Video
}

struct Video: Codable {
    let encodeURL: String
    enum CodingKeys: String, CodingKey {
        case encodeURL = "encodeUrl"
    }
}
typealias VideoData = [RootElement]
