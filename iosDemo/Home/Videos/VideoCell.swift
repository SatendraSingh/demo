//
//  VideoCell.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit
import AVKit
class VideoCell: UICollectionViewCell {
   // @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var thumbnail: UIImageView!
    let playerViewController = AVPlayerViewController()
    var player : AVPlayer?
    func configureVideo(_ url : String?){
        //print("fjdkjkdf",url)
        player = AVPlayer(url: URL(string: url ?? "https://wolverine.raywenderlich.com/content/ios/tutorials/video_streaming/foxVillage.m3u8")!)
        //obj that displayes the video content from a player object
        playerViewController.player = player
        //begin playback of the item
        //add the view to the uiview as a subview
        //playerViewController.view.bounds = videoView.bounds
        //videoView.addSubview(playerViewController.view)
        thumbnail.addSubview(playerViewController.view)
       
    }
    func configureVideoCell(_ url : String?){
        player = AVPlayer(url: URL(string: url ?? "https://wolverine.raywenderlich.com/content/ios/tutorials/video_streaming/foxVillage.m3u8")!)
        playerViewController.player = player
        DispatchQueue.global().async {
            var image = self.createThumbnailOfVideoFromRemoteUrl(url ?? "https://wolverine.raywenderlich.com/content/ios/tutorials/video_streaming/foxVillage.m3u8")
            DispatchQueue.main.async {
                if let image = image{
                    self.thumbnail.image = image
                }
            }
        }
        
    }
    func createThumbnailOfVideoFromRemoteUrl(_ url: String) -> UIImage? {
        //create an asset
        let asset = AVAsset(url: URL(string: url)!)
        //AVAssetImageGenerator uses the default enabled video track(s) to generate images.
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        //if this is not set, the thumbnail may come out sideways
        assetImgGenerate.appliesPreferredTrackTransform = true
        //Can set this to improve performance if target size is known before hand
        assetImgGenerate.maximumSize = CGSize(width: 200,height: 200)
        //define where in the video  we want to generate a thumbnail from
    let time = CMTimeMakeWithSeconds(1.0,preferredTimescale: 600)
        do {
            //This function will go into our media item and create a core graphics image at the time we specify using our CMTime
            let img = try
    assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            print(error.localizedDescription)
            return nil
            }
    }

    func play(){
        //thumbnail.addSubview(playerViewController.view)
        player?.play()
    }
}
