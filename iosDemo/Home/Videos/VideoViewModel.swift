//
//  VideoViewModel.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
struct videoViewModel{
    var category : String?
    var url  = [urlString]()
}
struct urlString{
    var url : String?
}
class VideoViewModel{
    var viewModelData = [videoViewModel]()
    var videoData : videoViewModel?
    var jsonData : VideoData?
    var URLString : urlString?
    func getVideoData(_ completionHandler : @escaping () -> Void){
        var data : Data?
        if let path = Bundle.main.path(forResource: "VideoData.json", ofType: nil){
            let fileUrl = URL(fileURLWithPath: path)
            do{
               data = try Data(contentsOf: fileUrl)
            }
            catch{
                print(error)
            }
            jsonData = try? JSONDecoder().decode(VideoData.self, from: data!)
            if let value = jsonData {
                for elem in value{
                    videoData = videoViewModel()
                    URLString = urlString()
                    videoData?.category = elem.title
                    for videoUrl in elem.nodes{
                        URLString!.url = videoUrl.video.encodeURL
                        videoData?.url.append(URLString!)
                    }
                    viewModelData.append(videoData!)
                }
                
            }
            
        }
        completionHandler()
    }
    
}
