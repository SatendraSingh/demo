//
//  WebViewController.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 10/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit
import WebKit
class WebViewController: UIViewController, WKNavigationDelegate{
    var urlString : String?
    var webView : WKWebView!
    var activityIndicator : UIActivityIndicatorView!
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: urlString ?? "https://google.com")
        webView.load(URLRequest(url: url!))
        webView.allowsBackForwardNavigationGestures = true
        // Do any additional setup after loading the view.
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.color = UIColor.darkGray
        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 200.0, height: 200.0);
        activityIndicator.center = webView.center
        activityIndicator.hidesWhenStopped = true
        webView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
          activityIndicator.stopAnimating()
      }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
