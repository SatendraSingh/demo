//
//  NewsViewModel.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
enum thumbnailType: Int {
    case thumbnail = 1
    case normal = 2
}
struct ViewModel{
    //properties to store data from model
    var title : String?
    var abstract: String?
    var articleLink: String?
    var author: String?
    var publishDate: String?
    var multimedia: String?
}
extension ViewModel : viewModelProtocol{
    var image: String {
        return multimedia ?? ""
    }
    
    var heading: String {
        return title ?? ""
    }
    
    var description: String {
        return author ?? ""
    }
}
protocol viewModelClassProtocol : AnyObject{
    var viewModelData : [viewModelProtocol] {get}
    func getData(completionHandler : @escaping () -> Void)
}
class NewsViewModel : viewModelClassProtocol{
    var viewModelData: [viewModelProtocol] = [ViewModel]()
    //array of type viewmodel to store the array of data coming from the model
    //var viewModelData = [ViewModel]()
    
    //function to get news data from the api
    func getData(completionHandler: @escaping () -> Void){
        let networkManager = NetworkManager()
        let urlString = "https://api.nytimes.com/svc/topstories/v2/world.json?api-key=tWSCptllMGdsnO3ON4RnEejTtawy0VU8"
        //url from urlstring
        guard let url = URL(string: urlString) else {
            return
        }
        //use the network manager to get data
        //define callback to handle data
        networkManager.getData(URL: url){
            (data : NewsDataModel?) in
            guard let newsHomeData = data else {return}
                //parse JSON data to store it as a newsDataModel type
               // let newsHomeData = try JSONDecoder().decode(NewsDataModel.self, from: data)
                var viewModel : ViewModel
                for newsData in newsHomeData.results! {
                    viewModel = ViewModel()
                    viewModel.title = newsData.title
                    viewModel.abstract = newsData.abstract
                    viewModel.articleLink = newsData.articleLink
                    viewModel.author = newsData.author
                    viewModel.publishDate = newsData.publishDate
                    let indexType: Int = thumbnailType.thumbnail.rawValue
                     if newsData.multimedia!.count > 0 {
                         viewModel.multimedia = newsData.multimedia?[indexType].url
                     }
                    self.viewModelData.append(viewModel)
                }
                completionHandler()
        }
        
    }
}
