//
//  NewsViewCell.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit

class NewsViewCell: UITableViewCell {
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
        func configureCellWithModel(_ model : [viewModelProtocol], indexPath : IndexPath){
            author.text = model[indexPath.row].description
            newsTitle.text = model[indexPath.row].heading
            loadImage(model[indexPath.row].image)
        }
        func loadImage(_ url: String){
            newsImage.image =  UIImage(named:"News1.jpg")
               guard url.count > 0 else { return }
               DispatchQueue.global().async {
                   let url = URL(string: url)
                   let data = try? Data(contentsOf: url!)
                   DispatchQueue.main.async {
                       if let dta = data {
                        self.newsImage.image = UIImage(data: dta)
                       }
                   }
               }
           }
    }
