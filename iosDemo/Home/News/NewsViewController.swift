//
//  NewsViewController.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit

class NewsViewController: UITableViewController {
    //let viewModel = NewsViewModel()
    var viewModel : viewModelClassProtocol?
    var activityIndicator : UIActivityIndicatorView?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        activityIndicator = UIActivityIndicatorView()
        activityIndicator!.color = UIColor.darkGray
        activityIndicator!.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        activityIndicator!.hidesWhenStopped = true
        activityIndicator!.center = parent!.view.center
        view?.addSubview(activityIndicator!)
        activityIndicator!.startAnimating()
        viewModel!.getData{
              DispatchQueue.main.async {
                self.activityIndicator!.stopAnimating()
                self.tableView.reloadData()
            }
        }
    }
    func selectViewModel(_ index : Int) -> viewModelClassProtocol? {
        switch index {
        case 0:
            return NewsViewModel()
        case 1:
            return FoodViewModel()
        default:
            return nil
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel!.viewModelData.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell", for: indexPath) as! NewsViewCell
               //confifure the cell with viewmodel data
               cell.configureCellWithModel(viewModel!.viewModelData, indexPath: indexPath)
               return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel is NewsViewModel{
            let vc = self.storyboard?.instantiateViewController(identifier: "newsDetails") as! NewsDetailsController
            vc.modalPresentationStyle = .fullScreen
            vc.viewModel = viewModel?.viewModelData[indexPath.row] as! ViewModel
            //parent!.navigationController!.pushViewController(vc, animated: true)
            self.navigationController!.pushViewController(vc, animated: true)
           // self.present(vc, animated: true, completion: nil)
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
