//
//  NewsDataModel.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
struct NewsDataModel: Decodable {
    let results : [results]?
}
struct results: Decodable {
    let title : String?
    let abstract: String?
    let articleLink: String?
    let author: String?
    let publishDate: String?
    let multimedia: [multimedia]?
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case abstract = "abstract"
        case articleLink = "url"
        case author = "byline"
        case publishDate = "published_date"
        case multimedia = "multimedia"
    }
}

struct multimedia: Decodable {
    let url: String?
    let format: String?
}
