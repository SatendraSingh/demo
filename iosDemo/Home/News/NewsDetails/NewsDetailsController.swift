//
//  NewsDetailsController.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 09/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit

class NewsDetailsController: UIViewController {
    var viewModel = ViewModel()
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var abstract: UILabel!
     @IBOutlet weak var abstractLink: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        populateData()
        // Do any additional setup after loading the view.
    }
    func populateData() {
        newsTitle.text = viewModel.title
        author.text = viewModel.author
        abstract.text = viewModel.abstract
        abstractLink.setTitle(viewModel.articleLink, for: .normal)
        if let multimedia = viewModel.multimedia {
        loadImage(multimedia)
        }
        //loadImage(viewModel.multimedia!)
    }
    func loadImage(_ url: String){
      newsImage.image =  UIImage(named:"News1.jpg")
      guard url.count > 0 else { return }
      DispatchQueue.global().async {
          let imageString = url.replacingOccurrences(of: "thumbLarge", with: "mediumThreeByTwo210")
          let url = URL(string: imageString)
          let data = try? Data(contentsOf: url!)
          DispatchQueue.main.async {
              if let dta = data {
                  self.newsImage.image = UIImage(data: dta)
                }
            }
        }
    }
    
    
    @IBAction func linkClicked(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(identifier: "webView") as! WebViewController
        viewController.urlString = abstractLink.title(for: .normal)
        //print(self.navigationController)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
