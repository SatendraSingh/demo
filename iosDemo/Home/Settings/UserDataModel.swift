//
//  UserDataModel.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 14/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
struct UserDataModel{
    var firstName : String?
    var lastName : String?
    var email : String?
    var profilePicture : NSData?
}
