//
//  SettingsViewMode;.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 14/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
import UIKit
//the two sections of the table
enum profileViewModelItemType{
    case profilePicture
    case details
}
struct Profile{
    var firstName : String?
    var email : String?
    var lastname : String?
    var profilePicture : NSData?
}
class SettingsViewModel{
    var user = Profile()
    var dataManager = DataManager()
    func getData(email : String,_ completion : @escaping ()->Void){
        dataManager.getLoggedInUserDetails(email)
        user.firstName = DataManager.UserDetails.firstName ?? ""
        user.lastname = DataManager.UserDetails.lastName ?? ""
        user.email = DataManager.UserDetails.email ?? ""
        completion()
    }
    func updateData(_ email : String, _ firstName : String, _ lastName : String, _ image : UIImage?) -> Bool? {
        return dataManager.update(email,firstName, lastName, prepareImage(image: image))
    }
    func prepareImage(image : UIImage?) -> Data?{
        guard let profilePicture = image else{
            return nil
        }
        //returns a data object containing the specified imgin JPEG format
        guard let imageData = profilePicture.jpegData(compressionQuality: 1) else{
            return nil
        }
        return imageData
    }
}
