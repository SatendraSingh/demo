//
//  SettingsViewController.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 13/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var Email: UITextView!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var lastName: UITextView!
    @IBOutlet weak var firstName: UITextView!
    @IBOutlet weak var camera: UIButton!
    @IBOutlet weak var EditBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    let pickerController = UIImagePickerController()
    var viewModel = SettingsViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerController.delegate = self
        pickerController.allowsEditing = true
        pickerController.mediaTypes = ["public.image"]
        //get the media types that are available for the source type with this
       // print("ffdff",UIImagePickerController.availableMediaTypes(for: .photoLibrary))
        saveBtn.isHidden = true
        EditBtn.setTitle("Edit", for: .normal)
        //an implicit clipping mask is created that matches the bounds of the layer and any corner radius effects
              profilePicture.layer.masksToBounds = true
              //to get a circular img from a sqaured one, radius is set to half the width of the img view
              profilePicture.layer.cornerRadius = profilePicture.bounds.width/2
              profilePicture.image = UIImage(named: "default")
        camera.isHidden = true
       /* viewModel.getData(email: UserDefaults.standard.string(forKey : "user")!){
            DispatchQueue.main.async {
                self.Email.text = self.viewModel.user.email
                self.firstName.text = self.viewModel.user.firstName
                self.lastName.text = self.viewModel.user.lastname
                self.tableView.reloadData()
            }
        }*/
        self.Email.text = "aishwaryasrao97@gmail.com"
        
        // Do any additional setup after loading the view.
    }
 
    @IBAction func EditBtnClicked(_ sender: Any) {
        Email.isEditable = true
        firstName.isEditable = true
        lastName.isEditable = true
        //let button = UIBarButtonItem(barButtonSystemItem: .camera, target: nil, action: nil)
        camera.isHidden = false
        saveBtn.isHidden = false
        EditBtn.isHidden = true
    }
    
    @IBAction func cameraClicked(_ sender: Any) {
        var alertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Open Camera", style: .default, handler: { (UIAlertAction) in
            self.pickerController.sourceType = .camera
            self.present(self.pickerController, animated: true, completion: {})
        }))
        alertController.addAction(UIAlertAction(title: "From gallery", style: .default, handler: { (UIAlertAction) in
            self.pickerController.sourceType = .photoLibrary
            self.present(self.pickerController, animated: true, completion: {})
        }))
        present(alertController, animated: true, completion: nil)
    }
    
    //method that's called when the user picks a media item
    //tells the delegate the user picked an image or movie
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.editedImage] as? UIImage {
            profilePicture.image = pickedImage
            profilePicture.contentMode = .scaleAspectFit
        }
        //dismissed the vc presented modally by the view controller
        //dismiss the image picker controller
        dismiss(animated: true, completion: nil)
    }
    
    //tells the delegate the user cancelled the pick operation
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveClicked(_ sender: Any) {
        Email.isEditable = false
             firstName.isEditable = false
             lastName.isEditable = false
             //let button = UIBarButtonItem(barButtonSystemItem: .camera, target: nil, action: nil)
             camera.isHidden = true
        saveBtn.isHidden = true
        EditBtn.isHidden = false
        if let status = viewModel.updateData(Email.text!, firstName.text!, lastName.text!, profilePicture.image){
            var alertController = UIAlertController(title: "Success", message: "Details updated successfully", preferredStyle: .alert)
                   alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
