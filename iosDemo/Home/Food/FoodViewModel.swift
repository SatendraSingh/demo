//
//  FoodViewModel.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
protocol viewModelProtocol {
    var image : String {get}
    var heading : String {get}
    var description : String {get}
}
struct foodViewModel{
    var itemName : String?
    var vendorName: String?
    var price: String?
    var imageUrl: String?
}
extension foodViewModel : viewModelProtocol{
    var image: String {
        return imageUrl ?? ""
    }
    
    var heading: String {
        return itemName ?? ""
    }
    
    var description: String {
        return vendorName ?? ""
    }
}
class FoodViewModel : viewModelClassProtocol{
    var viewModelData: [viewModelProtocol] = [foodViewModel]()
    //get data
    func getData(completionHandler : @escaping () -> Void){
        let networkManager = NetworkManager()
        let urlString = "https://betaapi.scootsy.com/exam.json"
        //url from urlstring
        guard let url = URL(string: urlString) else{
            return
        }
        networkManager.getData(URL: url){
            (data : DataModel?) in
            guard let foodModelData = data else {return}
                let dataArray = foodModelData.mainContent!.data!
                for foodData in dataArray{
                    for item in foodData.itemDetails!{
                        var viewModel = foodViewModel()
                        viewModel.itemName = item.itemName
                        viewModel.price = item.price
                        viewModel.imageUrl = item.imageUrl
                        viewModel.vendorName = item.vendorName
                        self.viewModelData.append(viewModel)
                    }
                }
                completionHandler()
        }
    }
}
