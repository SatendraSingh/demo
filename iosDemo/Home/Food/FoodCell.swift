//
//  FoodCell.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit

class FoodCell: UICollectionViewCell {
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var vendor: UILabel!
    func configureCellWithModel(_ model : [foodViewModel], indexPath: IndexPath){
        itemName.text = model[indexPath.row].itemName
        vendor.text = model[indexPath.row].vendorName
        loadImage(model[indexPath.row].imageUrl ?? "")
    }
    func loadImage(_ url : String){
        itemImage.image =  UIImage(named:"News1.jpg")
        guard url.count > 0 else { return }
        DispatchQueue.global().async {
            let url = URL(string: url)
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                if let dta = data {
                    self.itemImage.image = UIImage(data: dta)
                }
            }
        }
    }
}
