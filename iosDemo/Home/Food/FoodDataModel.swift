//
//  FoodDataModel.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
struct DataModel: Decodable {
    let mainContent : mainContent?
    
    enum CodingKeys : String, CodingKey {
        case mainContent = "main_content"
    }
}

struct mainContent: Decodable {
    let data: [data]?
}

struct data: Decodable {
    let vendorDetails: vendorDetails?
    let itemDetails : [itemDetails]?
    
    enum CodingKeys : String, CodingKey {
        case vendorDetails = "vendor_details"
        case itemDetails = "item_details"
    }
}

struct vendorDetails: Decodable {
    let vendorName : String?
    let deliveryTime: String?
    let openingTime: [openingTime]?
    
    enum CodingKeys: String, CodingKey {
        case vendorName = "vendor_name"
        case deliveryTime = "vendor_order_min_time"
        case openingTime = "openingTime"
    }
}

struct openingTime: Decodable {
    let start : String?
    let end: String?
}

struct itemDetails: Decodable {
    let itemName : String?
    let vendorName: String?
    let price: String?
    let imageUrl: String?
    let imageHeight: String?
    
    enum CodingKeys: String, CodingKey {
        case itemName = "item_name"
        case vendorName = "vendor_name"
        case price = "price"
        case imageUrl = "app_image"
        case imageHeight = "app_image_height"
    }
}
