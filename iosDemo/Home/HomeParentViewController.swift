//
//  HomeParentViewController.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit

class HomeParentViewController: UIViewController {
    var currentViewController : UIViewController?
    @IBOutlet weak var tabs: UISegmentedControl!
    @IBOutlet weak var contentView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //display news tab as default
        displayView(0)
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let currentViewController = currentViewController{
            //Notifies the view controller that its view is about to be removed from a view hierarchy
        currentViewController.viewWillDisappear(animated)
        }
        
    }
    //lazy variables are created using the function that's specified, only when they're required
    //if it's not requested, it's never run, so it helps save processing time
    //lazy prop are computed only when they're first needed, after which the value is saved
    lazy var newsChildTab : UIViewController? = {
        let newsChildTab = self.storyboard?.instantiateViewController(withIdentifier : "NewsView") as! NewsViewController
        newsChildTab.viewModel = newsChildTab.selectViewModel(0)
        return newsChildTab
    }()
    
    lazy var foodChildTab : UIViewController? = {
        let foodChildTab = self.storyboard?.instantiateViewController(identifier: "NewsView") as! NewsViewController
        foodChildTab.viewModel = foodChildTab.selectViewModel(1)
        return foodChildTab
    }()
    
    lazy var videoChildTab : UIViewController? = {
        let videoChildTab = self.storyboard?.instantiateViewController(identifier: "videoView")
        return videoChildTab
    }()
    
    @IBAction func indexChanged(_ sender: UISegmentedControl){
        //unlink the current view from the super view
        self.currentViewController!.view.removeFromSuperview()
        //remove the current active vc from the parent view controller, i.e the home parent vc
        self.currentViewController!.removeFromParent()
        //display the appropriate view controller based on the segment selected
        displayView(sender.selectedSegmentIndex)
    }
    
    func selectViewController(_ index : Int) -> UIViewController?{
        switch index {
        case 0 : return newsChildTab
        case 1 : return foodChildTab
        case 2 : return videoChildTab
        default : return nil
        }
    }
    
    func displayView(_ index : Int){
        if let vc = selectViewController(index){
            //add the view controller as the child of the parent controller
            self.addChild(vc)
          // Called after the view controller is added or removed from a container view controller.
            vc.didMove(toParent: self)
            //the bounds of the view of the vc are set to the bounds of the UIIView of the parent controller
            vc.view.frame = self.contentView.bounds
            //add the view of the instantiated view controller as the subview of the uiview of the parent
            self.contentView.addSubview(vc.view)
            self.currentViewController = vc
        }
    }
    // MARK: - Navigation
     
     /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

