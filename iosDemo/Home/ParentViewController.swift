//
//  ParentViewController.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 13/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit

class ParentViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let rootVc = self.storyboard?.instantiateViewController(withIdentifier : "navController")
        let newsVC = self.storyboard?.instantiateViewController(withIdentifier : "NewsView") as! NewsViewController
        newsVC.viewModel = newsVC.selectViewModel(0)
        let newsItem = UITabBarItem()
        newsItem.image = UIImage(named: "news.png")
        newsItem.title = "News"
        //newsVC.tabBarItem = newsItem
        rootVc?.tabBarItem = newsItem
        rootVc?.addChild(newsVC)
        let foodVC = self.storyboard?.instantiateViewController(withIdentifier : "NewsView") as! NewsViewController
        foodVC.viewModel = newsVC.selectViewModel(1)
        let foodItem = UITabBarItem()
        foodItem.image = UIImage(named: "Food.png")
        foodItem.title = "Food"
        foodVC.tabBarItem = foodItem
        let videoVC = self.storyboard?.instantiateViewController(identifier: "videoView")
        let videoItem = UITabBarItem()
        videoItem.title = "Videos"
        videoItem.image = UIImage(named: "Video")
        videoVC?.tabBarItem = videoItem
        let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC")
        let settingsItem = UITabBarItem()
        settingsItem.title = "Settings"
        settingsItem.image = UIImage(named: "Settings")
        settingsVC?.tabBarItem = settingsItem
        self.viewControllers = [rootVc!, foodVC, videoVC!, settingsVC!]
        self.selectedViewController = rootVc!
        print("ghvhh")
        // Do any additional setup after loading the view.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
