//
//  ViewController.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 06/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
     var gradientLayer : CAGradientLayer = CAGradientLayer()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //print(UserDefaults.standard.bool(forKey: "loggedIn"))
        UserDefaults.standard.set(true, forKey: "loggedIn")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createGradientLayer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //Trying to do this in viewDidLoad results in an error, as we're trying to display a new VC even befoer the current one is displayed. We're trying to push two controllers onto the stack
        //present the home controller when the user is logged in
        if let viewController = Switcher.updateViewController(){
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }
    }
    func createGradientLayer() {
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor.darkGray.cgColor, UIColor.lightGray.cgColor]
           self.view.layer.insertSublayer(gradientLayer, at: 0)
       }
    @IBAction func login(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(identifier: "loginVC")
        viewController?.modalPresentationStyle = .fullScreen
        self.present(viewController!, animated: true, completion: nil)
    }
    @IBAction func Register(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(identifier: "registerVC")
               viewController?.modalPresentationStyle = .fullScreen
               self.present(viewController!, animated: true, completion: nil)
    }
}

