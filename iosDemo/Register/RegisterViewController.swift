//
//  RegisterViewController.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 06/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit
import CoreData
class RegisterViewController: UIViewController, UITextFieldDelegate {
    var dataManager = DataManager()
    var gradientLayer : CAGradientLayer = CAGradientLayer()
    //outlets
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var emailError: UILabel!
    @IBOutlet weak var lastNameError: UILabel!
    @IBOutlet weak var firstNameError: UILabel!
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var registerBtn: UIButton!
    var bottomlayer : CALayer?
    //view Model instance
    var registerViewModel = RegisterViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        email.delegate = self
        lastName.delegate =  self
        firstName.delegate = self
        password.delegate = self
        addBottomLayer(forTextField: email)
        addBottomLayer(forTextField: password)
        addBottomLayer(forTextField: firstName)
        addBottomLayer(forTextField: lastName)
        registerBtn.setTitleColor(UIColor.darkGray, for: .disabled)
        registerBtn.setTitleColor(UIColor.black, for: .normal)
        dataManager.retrieveData()
        //registerBtn.isEnabled = false
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
            registerBtn.isEnabled = false
           createGradientLayer()
       }
    //add bottom border for text field
    func addBottomLayer(forTextField textField : UITextField){
        bottomlayer = CALayer()
        //height -- the thickness of the line
        //width -- the length of the line
        //x,y -- specify the co-ordinates of the rectangle
        bottomlayer!.frame = CGRect(x: 0.0, y: textField.frame.height+2, width: textField.frame.width, height: 1.0)
        bottomlayer!.backgroundColor = UIColor.black.cgColor
        textField.layer.addSublayer(bottomlayer!)
    }
   
    func createGradientLayer() {
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor.lightGray.cgColor, UIColor.darkGray.cgColor]
           self.view.layer.insertSublayer(gradientLayer, at: 0)
       }

   func textFieldDidEndEditing(_ textField: UITextField) {
    let validationMessage = registerViewModel.validateInput(textField.tag, textField.text)
    setError(textField,validationMessage)
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        guard emailError.text!.isEmpty,!email.text!.isEmpty, !password.text!.isEmpty, !firstName.text!.isEmpty,
            !lastName.text!.isEmpty
        else{
                registerBtn.isEnabled = false
                return
        }
        registerBtn.isEnabled = true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        var errors = [0 : emailError, 1 : passwordError, 2 : firstNameError, 3 : lastNameError]
        let errorLabel = errors[textField.tag] as! UILabel
        errorLabel.text = ""
    }
    func setError(_ textField : UITextField, _ message : String){
        //if !message.isEmpty{
           // let alertController = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertController.Style.alert)
            //let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
           // alertController.addAction(action)
            //presents the alert controller
           // present(alertController, animated: true, completion: nil)
            //emailError.text = message
        var errors = [0 : emailError, 1 : passwordError, 2 : firstNameError, 3 : lastNameError]
        let errorLabel = errors[textField.tag] as! UILabel
        errorLabel.text = message
        //}
    }
    func presentAlertController(_ message : String){
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    @IBAction func Register(_ sender: Any) {
        //save data
        if let val = dataManager.save(email.text!,password.text!,firstName.text!,lastName.text!){
            if val {
            let viewController = self.storyboard?.instantiateViewController(identifier: "loginVC")
            viewController?.modalPresentationStyle = .fullScreen
            self.present(viewController!, animated: true, completion: nil)
            }
            else {
                presentAlertController("Try again in some time")
            }
        }
        else{
            presentAlertController("An account already exists")
        }
    }
    
    
    @IBAction func login(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(identifier: "loginVC")
               viewController?.modalPresentationStyle = .fullScreen
               self.present(viewController!, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
