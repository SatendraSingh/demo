//
//  RegisterViewModel.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 06/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//
import UIKit
import Foundation
struct viewModel{
    var email : String?
    var password : String?
    var firstName : String?
    var lastName : String?
}
class RegisterViewModel{
    var typeToValidate = [0 : ValidateType.email, 1 : ValidateType.password, 2 : ValidateType.firstName, 3 :ValidateType.lastName]
    var regViewModel = viewModel()
    var validation = Validation()
    func validateInput(_ inputField : Int, _ input : String?) -> String{
        let validationMessage  = validation.validate(values: (typeToValidate[inputField] ?? ValidateType.email, input ?? ""))
        switch validationMessage {
        case .success:
            return ""
        case .failure( _, let alertMessage):
            return alertMessage.rawValue
        }
    }
}
