//
//  loginViewModel.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 06/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
struct loginViewModel{
    var email : String?
    var password : String?
}
class LoginViewModel{
     var typeToValidate = [0 : ValidateType.email]
    var validation = Validation()
    func validateInput(_ inputField : Int, _ input : String?) -> String{
        guard inputField == 0 else {
            return ""
        }
        let validationMessage = validation.validate(values: (ValidateType.email, input ?? ""))
        switch validationMessage {
        case .success:
            return ""
        case .failure( _, let alertMessage):
            return alertMessage.rawValue
        }
    }
}
