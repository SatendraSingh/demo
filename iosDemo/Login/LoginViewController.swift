//
//  LoginViewController.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 06/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    var dataManager = DataManager()
    var loginViewModel = LoginViewModel()
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var loginBtn: UIButton!

    @IBOutlet weak var errorLabel: UILabel!
    override func viewDidLoad() {
        loginBtn.setTitleColor(UIColor.lightGray, for: .disabled)
        loginBtn.setTitleColor(UIColor.black, for: .normal)
        loginBtn.isEnabled = false
        super.viewDidLoad()
        email.delegate = self
        password.delegate = self
        addBottomLayer(forTextField: email)
        addBottomLayer(forTextField: password)
        // Do any additional setup after loading the view.
    }
    //add bottom border for text fields
    func addBottomLayer(forTextField textField : UITextField){
        let bottomlayer = CALayer()
        //height -- the thickness of the line
        //width -- the length of the line
        //x,y -- specify the co-ordinates of the rectangle
        bottomlayer.frame = CGRect(x: 0.0, y: textField.frame.height+2, width: textField.frame.width, height: 1.0)
        bottomlayer.backgroundColor = UIColor.black.cgColor
        textField.layer.addSublayer(bottomlayer)
    }
    @IBAction func login(_ sender: Any) {
        let login = dataManager.verifyLogin(email.text!, password.text!)
        if login{
            UserDefaults.standard.set(true, forKey: "loggedIn")
            UserDefaults.standard.set(email.text!, forKey : "user")
             if let viewController = Switcher.updateViewController(){
                       viewController.modalPresentationStyle = .fullScreen
                       self.present(viewController, animated: true, completion: nil)
                   }
        }
        else{
            var alertController = UIAlertController(title: "", message: "Invalid Credentials", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
       // errorLabel.text = ""
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        errorLabel.text = ""
        guard !email.text!.isEmpty, !password.text!.isEmpty else{
            loginBtn.isEnabled = false
            return
        }
        loginBtn.isEnabled = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let validationMessage = loginViewModel.validateInput(textField.tag, textField.text)
        setError(textField,validationMessage)
    }
    
    func setError(_ textField : UITextField, _ message : String){
    if !message.isEmpty{
       /* let alertController = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alertController.addAction(action)
        //presents the alert controller
        present(alertController, animated: true, completion: nil)*/
        switch textField.tag{
        case 0 : errorLabel.text = message
        default : break
        }
    }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func Register(_ sender: Any) {
        let viewController = self.storyboard?.instantiateViewController(identifier: "registerVC")
               viewController?.modalPresentationStyle = .fullScreen
               self.present(viewController!, animated: true, completion: nil)
    }
}
