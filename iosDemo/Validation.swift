//
//  Validation.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 06/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
enum Alert{
    case success
    case error
}
enum Valid {
    case success
    case failure(Alert, alertMessages)
}
enum ValidateType {
    case email
    case firstName
    case lastName
    case password
}
enum RegEx: String {
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" // Email
    //password with minimum 8 digits having one uppercase charatcter,lowerase character, a number, a special character
    case password = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@$!%*?&])[A-Za-z0-9@$!%*?&]{8,}"
}
enum alertMessages : String{
    case inValidEmail = "InvalidEmail"
    case inValidPassword = "Invalid Password"
    case emptyEmail = "Email cannot be empty"
    case emptyFirstName = "First Name cannot be empty"
    case emptyLastName = "Last Name cannot be empty"
    case emptyPassword = "Password cannot be empty"
}
class Validation : NSObject{
    //takes multiple tuple values to validate
    func validate(values : (type : ValidateType, inputValue : String)...) -> Valid{
        for value in values{
            switch value.type{
            case .email:
                if let tempValue = isValidInput(value.inputValue, regEx: RegEx.email, alertMessage: (alertMessages.emptyEmail,alertMessages.inValidEmail)){
                    return tempValue
                }
            case .password:
                if let tempValue = isValidInput(value.inputValue, regEx: RegEx.password, alertMessage: (alertMessages.emptyPassword, alertMessages.inValidPassword)){
                    return tempValue
                }
            case .firstName:
                if let tempValue = isValidInput(value.inputValue, regEx: nil, alertMessage: (alertMessages.emptyFirstName, nil)){
                    return tempValue
                }
            case .lastName:
                if let tempValue = isValidInput(value.inputValue, regEx: nil, alertMessage: (alertMessages.emptyLastName, nil)){
                    return tempValue
                }
            }
        }
        return .success
    }
    
    func isValidInput(_ input : String, regEx : RegEx?, alertMessage : (emptyAlert : alertMessages, invalidInputAlert : alertMessages?)) -> Valid?{
        if input.isEmpty{
            return Valid.failure(.error, alertMessage.emptyAlert)
        }
        guard regEx != nil else{
            return .success
        }
        if !isValidRegEx(input, regEx!){
            return Valid.failure(.error, alertMessage.invalidInputAlert!)
        }
        return .success
    }
    
    func isValidRegEx(_ input : String, _ regEx : RegEx) -> Bool{
        if input.startIndex..<input.endIndex == input.range(of: regEx.rawValue, options: .regularExpression){
            print("Matched!")
            return true
        }
        return false
    }
}
