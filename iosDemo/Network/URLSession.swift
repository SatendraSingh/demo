//
//  URLSession.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
import UIKit
class UrlSession{
    //dataTask methods create tasks for response and data delivery
    //provide data asynchronously
    //data task expects a completionHandler -- escaping closure of the following type
    //datatask method returns a URLSessiondatatask
    //onCompletion is the type of the completion handler
    typealias onCompletion = (_ data: Data?, _ response: URLResponse?,_ error: Error?) -> ()
    func request(URL requestURL : URL, completion: @escaping onCompletion){
        URLSession.shared.dataTask(with: requestURL){
            data,response,error in
            //call the escaping completion handler
            completion(data,response,error)
        }.resume()
    }
}
