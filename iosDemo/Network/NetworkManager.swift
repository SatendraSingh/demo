//
//  NetworkManager.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 07/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
class NetworkManager{
   //  typealias onCompletion =
    //use urlsession
    let urlSession = UrlSession()
    func getData<T : Decodable>(URL requestURL: URL, _ completion : @escaping (T?) -> ()){
        urlSession.request(URL: requestURL){
            data,response,error in
            if error != nil {
                completion(nil)
            }
            if let response: HTTPURLResponse = response as? HTTPURLResponse {
                if response.statusCode == 200 {
                    do {
                    let ModelData = try JSONDecoder().decode(T.self,from: data!)
                         completion(ModelData)
                    }
                     catch{
                                  print("Failed to serialise")
                              }
                   
                } else {
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }
    }
}
