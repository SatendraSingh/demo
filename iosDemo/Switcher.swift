//
//  Switcher.swift
//  iosDemo
//
//  Created by MEP LAB 01 on 09/01/20.
//  Copyright © 2020 MEP LAB 01. All rights reserved.
//

import Foundation
import UIKit
class Switcher {
    static func updateViewController() -> UIViewController?{
        var rootVC : UIViewController?
        let status = UserDefaults.standard.bool(forKey: "loggedIn")
        if status == true{
           rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "tabBarController")
        }
        else{
            rootVC = nil
        }
        return rootVC
        
    }
}
